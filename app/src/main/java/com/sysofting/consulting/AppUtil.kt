package com.sysofting.consulting

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*

object AppUtil {

    private val timeStampFormat = SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA)
    var path: File? = null

    fun createAndSaveFileFromBase64Url(
        context: Context,
        url: String
    ): String {
        Toast.makeText(context, "파일을 다운로드합니다.", Toast.LENGTH_SHORT).show()
        path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        var fileType = url.substring(url.indexOf("/") + 1, url.indexOf(";"))
        if (fileType == "vnd.ms-excel") {
            fileType = "html"
        }
        var fileName = "EUBID_" + timeStampFormat.format(Date()) + "." + fileType
        val file = File(path, fileName)

        try {
            if (!file.exists()) file.createNewFile()
            val base64EncodedString = url.substring(url.indexOf(",") + 1)
            val decodedBytes = Base64.decode(base64EncodedString, Base64.DEFAULT)
            Timber.d("decodedBytes : $decodedBytes")
            val os: OutputStream = FileOutputStream(file)
            os.write(decodedBytes)
            os.flush()
            os.close()

            MediaScannerConnection.scanFile(
                context, arrayOf(file.toString()), null
            ) { path, uri ->
                Log.i("ExternalStorage", "Scanned $path:")
                Log.i("ExternalStorage", "-> uri=$uri")
            }
            Toast.makeText(context, "다운로드가 완료되었습니다.", Toast.LENGTH_SHORT).show()
        } catch (e: IOException) {
            Timber.w("Error: $e")
            Toast.makeText(context, R.string.error_downloading, Toast.LENGTH_LONG).show()
        }
        fileName = fileName.replace("vnd.ms-excel", "xls")
        return fileName
    }
}