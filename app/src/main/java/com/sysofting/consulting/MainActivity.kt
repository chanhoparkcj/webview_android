package com.sysofting.consulting

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.net.http.SslError
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.webkit.*
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import com.sysofting.consulting.AppUtil.createAndSaveFileFromBase64Url
import com.sysofting.consulting.AppUtil.path
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.net.URLDecoder
import kotlin.system.exitProcess

class MainActivity : AppCompatActivity() {

    private val downloadManager: DownloadManager by lazy {
        getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
    }
    private lateinit var valueCallback: ValueCallback<Array<Uri>>
    private lateinit var popupWebView: WebView
    private var isPopupVisible = false
    private var isDialogOn = false
    private var isNetworkOk = true
    private var currentUrl = ""
    private var downloadId: Long = 0
    private var downloadUrl = ""
    private var downloadFileName = ""
    private var openFolderCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        start()
        initMainWebView()
    }

    private fun start() {
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        if (0 != applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE) {
            WebView.setWebContentsDebuggingEnabled(true)
        }

        registerReceiver(onDownloadComplete, IntentFilter().apply {
            addAction(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
            addAction(DownloadManager.ACTION_NOTIFICATION_CLICKED)
        })

        onClick()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initMainWebView() {
        main_webview.webViewClient = WebViewClient()
        with(main_webview.settings) {
            javaScriptEnabled = true
            setSupportMultipleWindows(true)
            javaScriptCanOpenWindowsAutomatically = true
            loadWithOverviewMode = true
            cacheMode = WebSettings.LOAD_DEFAULT
            domStorageEnabled = true
            useWideViewPort = true // 화면 꽉차게
            textZoom = 100
        }
        main_webview.loadUrl(MAIN_URL)
        setMainWebViewClient()
        setMainWebChromeClient()
        setDownloadProcess(main_webview)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initPopupWebview(context: Context?) {
        popupWebView = WebView(context)
        popupWebView.visibility = View.GONE
        with(popupWebView) {
            with(settings) {
//                setSupportMultipleWindows(true)
                javaScriptEnabled = true
                javaScriptCanOpenWindowsAutomatically = true
                domStorageEnabled = true
                useWideViewPort = true // 화면 꽉차게
                textZoom = 100
            }
            layoutParams = ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT
            )
        }
    }

    private fun setMainWebViewClient() {
        main_webview.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                isNetworkOk = true
                main_loading_progress.visibility = View.VISIBLE
                super.onPageStarted(view, url, favicon)
            }

            /**SSL 에러 발생 시*/
            override fun onReceivedSslError(
                view: WebView?,
                handler: SslErrorHandler?,
                error: SslError?
            ) {
                handler?.proceed()
            }

            /**에러 발생 시(ex.네트워크 에러)*/
            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                error?.let {
                    if (it.description == "net::ERR_INTERNET_DISCONNECTED" || it.description == "net::ERR_ADDRESS_UNREACHABLE") {
                        isNetworkOk = false
                        main_webview.visibility = View.INVISIBLE
                        if (!isDialogOn) {
                            showNetworkDialog()
                            isDialogOn = true
                        }
                        return
                    }
                }
                main_webview.goBack()
            }

            override fun doUpdateVisitedHistory(
                view: WebView?,
                url: String?,
                isReload: Boolean
            ) {
                currentUrl = url.toString()
                super.doUpdateVisitedHistory(view, url, isReload)
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                return false
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                main_loading_progress.visibility = View.GONE
                if (!main_layout.isVisible && isNetworkOk) {
                    main_layout.visibility = View.VISIBLE
                }
                main_current_url_text.text = url
            }
        }
    }

    private fun setMainWebChromeClient() {
        main_webview.webChromeClient = object : WebChromeClient() {

            @SuppressLint("SetJavaScriptEnabled")
            override fun onCreateWindow(
                view: WebView?,
                isDialog: Boolean,
                isUserGesture: Boolean,
                resultMsg: Message?
            ): Boolean {
                val result = view?.hitTestResult
                val data = result?.extra
                val context = view?.context

                data?.let {
                    if (shouldLoadUrlOnWebview(it, null)) {
                        return false
                    }
                }

                initPopupWebview(context)
                setPopupWebChromeClient(popupWebView)
                setPopupWebViewClient(popupWebView)
                main_layout.addView(popupWebView)
                setDownloadProcess(popupWebView)
                val transport: WebView.WebViewTransport =
                    resultMsg?.obj as WebView.WebViewTransport
                transport.webView = popupWebView
                resultMsg.sendToTarget()
                return true


            }

            /**파일 업로드 시*/
            override fun onShowFileChooser(
                webView: WebView?,
                filePathCallback: ValueCallback<Array<Uri>>?,
                fileChooserParams: FileChooserParams?
            ): Boolean {

                filePathCallback?.let {
                    valueCallback = it
                }

                val intent = Intent(Intent.ACTION_GET_CONTENT)
                with(intent) {
                    addCategory(Intent.CATEGORY_OPENABLE)
                    setType("*/*")
                }
                startActivityForResult(intent, 0)
                return true
            }
        }

    }

    private fun setPopupWebViewClient(webview: WebView) {
        webview.webChromeClient = object : WebChromeClient() {

            override fun onCreateWindow(
                view: WebView?,
                isDialog: Boolean,
                isUserGesture: Boolean,
                resultMsg: Message?
            ): Boolean {
                view?.hitTestResult?.extra?.let {
                    openExternalBrowser(it)
                }
                return false
            }

            override fun onCloseWindow(window: WebView?) {
                super.onCloseWindow(window)
                main_layout.removeView(window)
                window?.removeAllViews()
                window?.destroy()
                isPopupVisible = false
            }
        }

    }

    private fun setPopupWebChromeClient(webview: WebView) {
        webview.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                url: String?
            ): Boolean {
                url?.let {
                    view?.visibility = View.VISIBLE
                    isPopupVisible = true
                    return false
                } ?: return true
            }

            //안드로이드 버전 7 이상
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                return shouldLoadUrlOnWebview(request?.url.toString(), view)
            }

            override fun onReceivedSslError(
                view: WebView?,
                handler: SslErrorHandler?,
                error: SslError?
            ) {
                handler?.proceed()
            }
        }
    }

    /**페이지 로드방식*/
    private fun shouldLoadUrlOnWebview(url: String, view: WebView?): Boolean {
        return when {
            (url.contains("viewMode=1")) && !isPopupVisible -> {
                view?.visibility = View.VISIBLE
                isPopupVisible = true
                false
            }
            url.contains("/boardFileDown/") || url.endsWith("DownloadSection") -> {
                loadWebview(url, popupWebView)
                true
            }
            url.contains(MAIN_URL) || url.contains("viewMode=0") -> {
                loadWebview(url, main_webview)
                removePopWebView(view)
                true
            }
            url.contains("/docsview/") -> {
                false
            }
            url.contains("viewMode=0") -> {
                openExternalBrowser(url)
                true
            }
            else -> {
                openExternalBrowser(url)
//                removePopWebView(view)
                true
            }
        }
    }

    /**다운로드리스너 설정*/
    private fun setDownloadProcess(webview: WebView) {
        webview.setDownloadListener { url, _, contentDisposition, mimeType, _ ->
            run {
                var decodeStr = URLDecoder.decode(contentDisposition, "euc-kr")
                val decodeList =
                    ArrayList(
                        listOf(
                            *decodeStr.split("=".toRegex()).toTypedArray()
                        )
                    )
                decodeStr = decodeList[decodeList.size - 1]
                decodeStr = decodeStr.replace("\"", "")
                decodeStr = decodeStr.replace("\u003b", "")
                if (decodeStr.isEmpty()) {
                    decodeStr = url.substringAfterLast("/")
                }
                downloadFileName = decodeStr
                downloadUrl = url
                setDownload()
            }
        }
    }

    /**메인웹뷰에 'url'로드*/
    private fun loadWebview(url: String, webview: WebView) {
        webview.loadUrl(url)
    }

    /**외부 브라우저로 열기*/
    private fun openExternalBrowser(url: String) {
        Intent(Intent.ACTION_VIEW, Uri.parse(url)).apply {
            startActivity(this)
        }
    }

    /**권한체크*/
    private fun hasPermission(context: Context, permissions: Array<String>): Boolean {
        for (permission in PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    /**권한요청*/
    private fun getPermission() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, 1000)
    }

    /**다운로드 작업 */
    private fun setDownload() {
        when {
            !hasPermission(this, PERMISSIONS) -> {
                getPermission()
            }
            else -> {
                when (downloadUrl.startsWith("data:")) {
                    true -> {
                        val fileName = createAndSaveFileFromBase64Url(this, downloadUrl)
                        val intent = Intent(Intent.ACTION_VIEW)
                        val dir = Uri.parse("file://${path.toString()}/$fileName")
                        intent.setDataAndType(dir, "text/html")
                        startActivity(intent)
                    }
                    false -> {
                        val request = DownloadManager.Request(Uri.parse(downloadUrl))
                            .setTitle(downloadFileName)
                            .setDescription("파일을 다운로드 중입니다.")
                            .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                            .setDestinationInExternalPublicDir(
                                Environment.DIRECTORY_DOWNLOADS,
                                downloadFileName
                            )
                            .setAllowedOverMetered(true)
                            .setAllowedOverRoaming(true)
                        downloadId = downloadManager.enqueue(request)
                        Toast.makeText(applicationContext, "파일을 다운로드 중입니다.", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        }
    }

    /**다운로드 이후 작업*/
    private var onDownloadComplete: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            openFolderCount = 0
            val id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE == intent.action && downloadId == id) {
                val query = DownloadManager.Query()
                query.setFilterById(id)
                val cursor: Cursor = downloadManager.query(query)
                if (!cursor.moveToFirst()) return

                val columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)
                when (cursor.getInt(columnIndex)) {
                    DownloadManager.STATUS_SUCCESSFUL -> {
                        when (openFolderCount) {
                            0 -> startActivity(Intent(DownloadManager.ACTION_VIEW_DOWNLOADS))
                        }
                        openFolderCount++
                    }
                    DownloadManager.STATUS_FAILED -> {
                        Toast.makeText(context, "다운로드에 실패했습니다.", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    /**네트워크 에러 처리 다이얼로그*/
    private fun showNetworkDialog() {
        AlertDialog.Builder(this@MainActivity).apply {
            setTitle("네트워크에 접속할 수 없습니다.")
            setMessage("네트워크 연결 상태를 확인해주세요.")
            setPositiveButton("새로고침") { _, _ ->
                main_webview.reload()
                isDialogOn = false
            }
            setNegativeButton("앱 종료") { _, _ ->
                finishAffinity()
                exitProcess(0)
            }
            create().apply {
                setCancelable(false)
                setCanceledOnTouchOutside(false)
                show()
            }
        }
    }

    /**팝업웹뷰 제거*/
    private fun removePopWebView(view: WebView?) {
        isPopupVisible = false
        main_layout.removeView(view)
        view?.removeAllViews()
        view?.destroy()
    }

    /**클릭 이벤트*/
    private fun onClick() {
        main_home_button.setOnClickListener { main_webview.loadUrl(MAIN_URL) }
        main_refresh_page_button.setOnClickListener { main_webview.reload() }
        main_previous_page_button.setOnClickListener { main_webview.goBack() }
        main_next_page_button.setOnClickListener { main_webview.goForward() }
    }

    /**백버튼 처리*/
    override fun onBackPressed() {
        when {
            isPopupVisible && popupWebView.canGoBack() -> {
                popupWebView.goBack()
            }
            /**현재 페이지가 팝업인 경우*/
            isPopupVisible -> {
                popupWebView.loadUrl("javascript:window.close();")
                isPopupVisible = false
            }
            /**홈 화면인 경우*/
            main_webview.originalUrl.equals(MAIN_URL, false) -> {
                super.onBackPressed()
            }
            /**현재 페이지가 홈이 아닌 경우*/
            main_webview.canGoBack() -> {
                main_webview.goBack()
            }
        }
    }

    /**파일 업로드 처리*/
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                valueCallback.onReceiveValue(
                    WebChromeClient.FileChooserParams.parseResult(
                        resultCode,
                        data
                    )
                )
            } else {
                valueCallback.onReceiveValue(data?.data?.let { arrayOf(it) })
            }

        }
    }

    companion object {
//        private const val MAIN_URL = "http://test.eubid.co.kr:8086/" //테스트 서버
        private const val MAIN_URL = "http://192.168.0.61:8086"//찬호주임님
//        private const val MAIN_URL = "http://192.168.0.22:8086"//명철씨
//        private const val MAIN_URL = "http://192.168.0.50:8086"//지현대리님
//        private const val MAIN_URL = "http://192.168.0.9:9107"//임원진
        private const val SERVER_HOST = "192.168.0.22"
        private val PERMISSIONS = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }
}